﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exProOpA
{
    class Program
    {
        const int importeMadridIberia=99;
        const int importeBarcelonaIberia = 105;
        const int importeAlicanteIberia = 110;
        const int importeMadridAir = 96;
        const int importeBarcelonaAir = 104;
        const int importeAlicanteAir = 111;
        static double importeTotal=0;

        static void Main(string[] args)
        {
            int ContadorMadrid = 0;
            int ContadorAlicante = 0;
            int ContadorBarcelona = 0;
            Boolean continuar = true;
            while (continuar)
            {
                Console.WriteLine("Escribe con que compañia quieres viajar I (Iberia) - A (Air Europa) - S (Para salir): ");
                char decision =Convert.ToChar(Console.ReadLine());
                switch (decision)
                {
                    case 'I':
                        Console.WriteLine("Escribe a qué pais quieres ir: M (Madrid) - B (Barcelona) - A (Alicante)");
                        char decision2 = Convert.ToChar(Console.ReadLine());
                        if (decision2 == 'M')
                            ContadorMadrid++;
                        if (decision2 == 'B')
                            ContadorBarcelona++;
                        if (decision2 == 'A')
                            ContadorAlicante++;

                        Peticiones(decision, decision2, ContadorMadrid, ContadorBarcelona, ContadorAlicante);
                        break;

                    case 'A':
                        Console.WriteLine("Escribe a qué pais quieres ir: M (Madrid) - B (Barcelona) - A (Alicante)");
                        char decision3= Convert.ToChar(Console.ReadLine());
                        if (decision3 == 'M')
                            ContadorMadrid++;
                        if (decision3 == 'B')
                            ContadorBarcelona++;
                        if (decision3 == 'A')
                            ContadorAlicante++;

                        Peticiones(decision, decision3, ContadorMadrid, ContadorBarcelona, ContadorAlicante);
                        break;

                    case 'S':
                        Console.WriteLine("Total de dinero recaudado= " + importeTotal);
                        Console.WriteLine("Numero de ventas a destino Madrid= " + ContadorMadrid);
                        Console.WriteLine("Numero de ventas a destino Barcelona= " + ContadorBarcelona);
                        Console.WriteLine("Numero de ventas a destino Alicante= " + ContadorAlicante);
                        continuar = false;
                        break;

                }
            }
        }

        private static void Peticiones(char decision1, char decision2, int contador1, int contador2, int contador3)
        {

            if (decision2 == 'M')
            {
                contador1++;
            }
                
            if (decision2 == 'B')
            {
                contador2++;
            }
                
            if (decision2 == 'A')
            {
                contador3++;
            }
                

            Console.WriteLine("Numero de billetes: ");
            int numBilletes = int.Parse(Console.ReadLine());

            Console.WriteLine("Eres residente en Canarias: S (Si) - N  (No)");
            char residente = Convert.ToChar(Console.ReadLine());
            Console.ReadLine();
            if (decision1 == 'I' && decision2 == 'M' && residente == 'S')
            {
                double ImporteTotal = importeMadridIberia * numBilletes-(importeMadridIberia*0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'I' && decision2 == 'M' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }

            if (decision1 == 'I' && decision2 == 'A' && residente == 'S')
            {
                double ImporteTotal = importeAlicanteIberia * numBilletes - (importeAlicanteIberia * 0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'I' && decision2 == 'A' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }

            if (decision1 == 'I' && decision2 == 'B' && residente == 'S')
            {
                double ImporteTotal = importeBarcelonaIberia * numBilletes - (importeBarcelonaIberia * 0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'I' && decision2 == 'B' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }

            if (decision1 == 'A' && decision2 == 'M' && residente == 'S')
            {
                double ImporteTotal = importeMadridAir * numBilletes - (importeMadridAir * 0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'A' && decision2 == 'M' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }

            if (decision1 == 'A' && decision2 == 'A' && residente == 'S')
            {
                double ImporteTotal = importeAlicanteAir * numBilletes - (importeAlicanteAir * 0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'A' && decision2 == 'A' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }

            if (decision1 == 'A' && decision2 == 'B' && residente == 'S')
            {
                double ImporteTotal = importeBarcelonaAir * numBilletes - (importeBarcelonaAir * 0.7);
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
            if (decision1 == 'A' && decision2 == 'B' && residente == 'N')
            {
                double ImporteTotal = importeMadridIberia * numBilletes;
                Console.WriteLine("El importe total del billete es= " + ImporteTotal);
                importeTotal = importeTotal + ImporteTotal;
            }
        }
    }
}
